import 'package:flutter/material.dart';
import 'package:marvel/src/pages/character_page.dart';
import 'package:marvel/src/pages/home_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Marvel App',
      initialRoute: '/',
      routes: {
        '/': (BuildContext contex) => HomePage(),
        'detalle': (BuildContext contex) => CharacterDetail(),
      },
    );
  }
}
