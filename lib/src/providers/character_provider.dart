import 'dart:async';
import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:http/http.dart' as http;

import 'package:marvel/src/models/character_model.dart';
import 'package:marvel/src/models/comic_model.dart';
import 'package:marvel/src/models/serie_model.dart';

class CharacterProvider {
  String _apiKey = '5fdcff6e91cbf280eddf8ad607a34615';
  String _privateKey = 'b48f64396a589bbacca13943ca7fca5c88e0e0cf';
  String _ts = '1';
  String _hash = ''; //md5(ts+privateKey+publicKey)
  String _url = 'gateway.marvel.com';
  int _offset = 0;
  bool _loadingCharacter = false;

  List<Character> _characterList = new List();
  final _chartactersStreamController =
      StreamController<List<Character>>.broadcast();

  Function(List<Character>) get characterListSink =>
      _chartactersStreamController.sink.add;

  Stream<List<Character>> get chartactersStream =>
      _chartactersStreamController.stream;

  void disposeStreams() {
    _chartactersStreamController?.close();
  }

  //listado de personajes
  Future<List<Character>> getCharacterList() async {
    _hash = this.generateMd5(_ts + _privateKey + _apiKey);
    if (_loadingCharacter) return [];

    _loadingCharacter = true;
    final url = Uri.https(_url, '/v1/public/characters', {
      'apikey': _apiKey,
      'ts': _ts,
      'hash': _hash,
      'limit': '20',
      'offset': _offset.toString()
    });

    _offset = _offset + 20;
    //print(_offset);
    final resp = await http.get(url);
    final decodeResp = json.decode(resp.body);

    //print(decodeResp['data']);
    final characters =
        new Characters.fromJsonList(decodeResp['data']['results']);
    //print(characters.items[0].thumbnailpath);
    final respon = characters.items;

    _characterList.addAll(respon);
    characterListSink(_characterList);

    _loadingCharacter = false;
    return respon;
  }

  //listado de comics
  Future<List<Comic>> getComics(String id) async {
    _hash = this.generateMd5(_ts + _privateKey + _apiKey);
    final url = Uri.https(_url, '/v1/public/characters/${id}/comics',
        {'apikey': _apiKey, 'ts': _ts, 'hash': _hash});

    final resp = await http.get(url);
    final decodeComics = json.decode(resp.body);
    final comics = new Comics.fromJsonList(decodeComics['data']['results']);

    return comics.items;
  }

  //listado de series
  Future<List<Serie>> getSeries(String id) async {
    _hash = this.generateMd5(_ts + _privateKey + _apiKey);
    final url = Uri.https(_url, '/v1/public/characters/${id}/series',
        {'apikey': _apiKey, 'ts': _ts, 'hash': _hash});

    final resp = await http.get(url);
    final decodeComics = json.decode(resp.body);
    final series = new Series.fromJsonList(decodeComics['data']['results']);

    return series.items;
  }

  String generateMd5(String data) {
    return md5.convert(utf8.encode(data)).toString();
  }
}
