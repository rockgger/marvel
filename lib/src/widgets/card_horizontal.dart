import 'package:flutter/material.dart';
import 'package:marvel/src/models/comic_model.dart';

class CardListHorizontal extends StatelessWidget {
  final List<Comic> comics;
  CardListHorizontal({@required this.comics});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 280,
      child: PageView.builder(
          pageSnapping: true,
          controller: PageController(initialPage: 1, viewportFraction: 0.4),
          itemCount: comics.length,
          itemBuilder: (BuildContext context, int index) =>
              _cardComics(context, comics[index])),
    );
  }

  Widget _cardComics(BuildContext context, Comic comic) {
    final cardTarjeta = Card(
      color: Colors.white,
      elevation: 0,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FadeInImage(
            image: NetworkImage(comic.thumbnailpath),
            placeholder: AssetImage('assets/loading_big.gif'),
            height: 240,
            width: double.infinity,
            fit: BoxFit.cover,
            alignment: Alignment.topCenter,
            fadeInDuration: Duration(microseconds: 5000),
          ),
          Container(
            padding: EdgeInsets.only(left: 10, right: 10, top: 10),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    comic.title,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontWeight: FontWeight.w500),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
    return cardTarjeta;
  }
}
