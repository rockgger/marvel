import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:marvel/src/models/character_model.dart';

//widget para listar personajes
class CardList extends StatelessWidget {
  final List<Character> characters;
  final Function nexCharacterList;

  CardList({@required this.characters, @required this.nexCharacterList});

  ScrollController _scrollController = new ScrollController();

  @override
  Widget build(BuildContext context) {
    _scrollController.addListener(() {
      if (_scrollController.position.pixels >=
          _scrollController.position.maxScrollExtent - 300) {
        nexCharacterList();
      }
    });
    return Container(
        child: GridView.builder(
            /* reverse: true, */
            controller: _scrollController,
            primary: false,
            padding: EdgeInsets.all(7),
            itemCount: characters.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisSpacing: 0,
              mainAxisSpacing: 7,
              crossAxisCount: 2,
            ),
            itemBuilder: (BuildContext context, int index) =>
                _cardCharacter(context, characters[index])));
  }

  Widget _cardCharacter(BuildContext context, Character character) {
    final cardTarjeta = Card(
      color: Colors.white,
      elevation: 0,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FadeInImage(
            image: NetworkImage(character.thumbnailpath),
            placeholder: AssetImage('assets/loading_big.gif'),
            height: 130,
            width: double.infinity,
            fit: BoxFit.cover,
            fadeInDuration: Duration(microseconds: 5000),
          ),
          Container(
            padding: EdgeInsets.only(left: 10, right: 10, top: 5),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    character.name,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontWeight: FontWeight.w500),
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.star_border, color: Colors.blue),
                  onPressed: () {},
                ),
              ],
            ),
          )
        ],
      ),
    );

    return GestureDetector(
      child: cardTarjeta,
      onTap: () {
        Navigator.pushNamed(context, 'detalle', arguments: character);
      },
    );
  }
}
