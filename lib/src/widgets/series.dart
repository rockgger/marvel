import 'package:flutter/material.dart';

import 'package:marvel/src/models/serie_model.dart';

class CardListHorizontalSeries extends StatelessWidget {
  final List<Serie> series;
  CardListHorizontalSeries({@required this.series});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 280,
      child: PageView.builder(
          pageSnapping: true,
          controller: PageController(initialPage: 1, viewportFraction: 0.4),
          itemCount: series.length,
          itemBuilder: (BuildContext context, int index) =>
              _cardSeriess(context, series[index])),
    );
  }

  Widget _cardSeriess(BuildContext context, Serie serie) {
    final cardTarjeta = Card(
      color: Colors.white,
      elevation: 0,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FadeInImage(
            image: NetworkImage(serie.thumbnailpath),
            placeholder: AssetImage('assets/loading_big.gif'),
            height: 240,
            width: double.infinity,
            fit: BoxFit.cover,
            alignment: Alignment.topCenter,
            fadeInDuration: Duration(microseconds: 5000),
          ),
          Container(
            padding: EdgeInsets.only(left: 10, right: 10, top: 10),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    serie.title,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontWeight: FontWeight.w500),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
    return cardTarjeta;
  }
}
