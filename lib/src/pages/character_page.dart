import 'package:flutter/material.dart';
import 'package:marvel/src/models/character_model.dart';
import 'package:marvel/src/providers/character_provider.dart';
import 'package:marvel/src/widgets/card_horizontal.dart';
import 'package:marvel/src/widgets/series.dart';

class CharacterDetail extends StatelessWidget {
  final characterProvider = new CharacterProvider();

  @override
  Widget build(BuildContext context) {
    final Character character = ModalRoute.of(context).settings.arguments;
    return Scaffold(
        backgroundColor: Colors.grey[100],
        body: CustomScrollView(
          slivers: [
            _createAppbar(character),
            SliverList(
              delegate: SliverChildListDelegate([
                SizedBox(
                  height: 10.0,
                ),
                _characterDetail(character),
                _characterComics(character),
                _characterSeries(character),
              ]),
            ),
          ],
        ));
  }

  Widget _createAppbar(Character character) {
    return SliverAppBar(
      elevation: 2.0,
      expandedHeight: 250.0,
      centerTitle: true,
      floating: false,
      pinned: true,
      backgroundColor: Colors.black,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        background: FadeInImage(
          image: NetworkImage(character.thumbnailpath),
          placeholder: AssetImage('assets/loading_big.gif'),
          width: double.infinity,
          fit: BoxFit.cover,
          fadeInDuration: Duration(microseconds: 150),
        ),
      ),
    );
  }

  Widget _characterDetail(Character character) {
    return Container(
      padding: EdgeInsets.all(7),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            character.name + ':',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
          Text(character.description, textAlign: TextAlign.justify)
        ],
      ),
    );
  }

  Widget _characterComics(Character character) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(7),
      child: Column(
        children: [
          Text(
            'Comics',
            style: TextStyle(
                fontWeight: FontWeight.bold, color: Colors.black, fontSize: 16),
            textAlign: TextAlign.left,
          ),
          FutureBuilder(
            future: characterProvider.getComics(character.id.toString()),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              //print(snapshot.data);
              if (snapshot.hasData) {
                return CardListHorizontal(comics: snapshot.data);
              } else {
                return Center(child: CircularProgressIndicator());
              }
            },
          )
        ],
      ),
    );
  }

  Widget _characterSeries(Character character) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(7),
      child: Column(
        children: [
          Text(
            'Series',
            style: TextStyle(
                fontWeight: FontWeight.bold, color: Colors.black, fontSize: 16),
            textAlign: TextAlign.left,
          ),
          FutureBuilder(
            future: characterProvider.getSeries(character.id.toString()),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              //print(snapshot.data);
              if (snapshot.hasData) {
                return CardListHorizontalSeries(series: snapshot.data);
              } else {
                return Center(child: CircularProgressIndicator());
              }
            },
          )
        ],
      ),
    );
  }
}
