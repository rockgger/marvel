import 'package:flutter/material.dart';
import 'package:marvel/src/providers/character_provider.dart';
import 'package:marvel/src/widgets/card_widget.dart';

class HomePage extends StatelessWidget {
  final characterProvider = new CharacterProvider();

  @override
  Widget build(BuildContext context) {
    characterProvider.getCharacterList();

    return Scaffold(
      backgroundColor: Colors.grey[100],
      appBar: AppBar(
          centerTitle: true,
          title: Text(
            'Lista de Personajes',
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white),
      body: Container(
        /* color: Color(0XFCFCFC), */
        child: _characterList(),
      ),
    );
  }

  //listar personajes
  Widget _characterList() {
    return StreamBuilder(
      stream: characterProvider.chartactersStream,
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasData) {
          //returnados los personajes
          return CardList(
            characters: snapshot.data,
            nexCharacterList: characterProvider.getCharacterList,
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
