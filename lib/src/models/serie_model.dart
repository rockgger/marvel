class Series {
  List<Serie> items = new List();
  Series();
  Series.fromJsonList(List<dynamic> jsonlist) {
    if (jsonlist == null) return;

    for (var jmap in jsonlist) {
      final serie = new Serie.fromJsonMap(jmap);
      items.add(serie);
    }
  }
}

class Serie {
  int id;
  String title;
  String description;
  String thumbnailpath;

  Serie({
    this.id,
    this.title,
    this.description,
    this.thumbnailpath,
  });

  Serie.fromJsonMap(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    description = json['description'];
    thumbnailpath =
        json['thumbnail']['path'] + '.' + json['thumbnail']['extension'];
  }
}
