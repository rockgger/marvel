class Comics {
  List<Comic> items = new List();
  Comics();
  Comics.fromJsonList(List<dynamic> jsonlist) {
    if (jsonlist == null) return;

    for (var jmap in jsonlist) {
      final comic = new Comic.fromJsonMap(jmap);
      items.add(comic);
    }
  }
}

class Comic {
  int id;
  String title;
  String description;
  String thumbnailpath;

  Comic({
    this.id,
    this.title,
    this.description,
    this.thumbnailpath,
  });

  Comic.fromJsonMap(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    description = json['description'];
    thumbnailpath =
        json['thumbnail']['path'] + '.' + json['thumbnail']['extension'];
  }
}
