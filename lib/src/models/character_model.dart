//import 'package:flutter/cupertino.dart';

class Characters {
  List<Character> items = new List();
  Characters();
  Characters.fromJsonList(List<dynamic> jsonlist) {
    if (jsonlist == null) return;

    for (var jmap in jsonlist) {
      final character = new Character.fromJsonMap(jmap);
      items.add(character);
    }
  }
}

class Character {
  int id;
  String name;
  String description;
  String thumbnailpath;

  Character({this.id, this.name, this.description, this.thumbnailpath});

  Character.fromJsonMap(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    thumbnailpath =
        json['thumbnail']['path'] + '.' + json['thumbnail']['extension'];
  }
}

class Thumbnail {
  String path;
  String extension;

  Thumbnail({
    this.path,
    this.extension,
  });
}

//Character.fromJo
